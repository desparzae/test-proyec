package com.evaluacion.banco.bci.servicios;

import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import java.util.ArrayList;
import java.util.List;

import org.junit.Test;
import org.junit.jupiter.api.BeforeEach;

import org.junit.jupiter.api.extension.ExtendWith;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.mockito.internal.verification.Times;
import org.mockito.invocation.InvocationOnMock;
import org.mockito.junit.MockitoJUnitRunner;
import org.mockito.stubbing.Answer;
import org.springframework.http.ResponseEntity;

import static org.mockito.Mockito.*;

import com.evaluacion.banco.bci.controller.ControllerUsuario;
import com.evaluacion.banco.bci.entity.Usuario;
import com.evaluacion.banco.bci.repository.IUsuarioRepo;
import static org.junit.jupiter.api.Assertions.*;


@RunWith(MockitoJUnitRunner.Silent.class)
public class UsuarioServicioImpleTest {

	@Mock
	UsuarioServicioImple usuarioImple;
	
	@Mock
	ControllerUsuario usuarioController;
	
	@Mock
	IUsuarioServicio usuarioServicio;
	
	@Mock
	IUsuarioRepo usuarioRepo;
	
    @BeforeEach
    void setUp() {
     MockitoAnnotations.initMocks(this);

    }
	
	
  @Test
  public void buscarUsuarioPorCorreoTest() {
    String correo = "daniel@test.cl";
    
    when(usuarioImple.buscarUsuarioPorCorreo(correo)).thenReturn(Datos.MOCK_USUARIO);
    verify(usuarioRepo, times(1));
     
  }


  @Test
  public void listaUsuariosTest() {
	  List<Usuario> listaUsuarios = new ArrayList<Usuario>();
	  listaUsuarios.add(Datos.MOCK_USUARIO);
	  
	  when(usuarioImple.listaUsuarios()).thenReturn(listaUsuarios);
	    verify(usuarioRepo, times(1));
  }

  @Test
  public void updateUsuarioTest() {

	  
	  when(usuarioImple.updateUsuario(Datos.MOCK_USUARIO)).thenReturn(Datos.MOCK_USUARIO);
	    verify(usuarioRepo, times(1));
  }

  @Test
  public void validaClaveTest() {
	  String claveCorrecta= "Haaaniel23";
			  
	  when(usuarioImple.validaClave(claveCorrecta)).thenReturn(true);
	   assertEquals(true, usuarioImple.validaClave(claveCorrecta));
       assertTrue(usuarioImple.validaClave(claveCorrecta));

    
  }

  @Test
  public void validaEmailTest() {
    
	  String eamilCorrecto= "juan@gaaa.cl";
	  
	  when(usuarioImple.validaEmail(eamilCorrecto)).thenReturn(true);
	   assertEquals(true, usuarioImple.validaEmail(eamilCorrecto));
       assertTrue(usuarioImple.validaEmail(eamilCorrecto));
  }
  
  @Test
  public void validaClaveTest2() {
	  String claveIncorrecta= "Haaaniel2322";
			  
	  when(usuarioImple.validaClave(claveIncorrecta)).thenReturn(false);
	   assertEquals(false, usuarioImple.validaClave(claveIncorrecta));
       assertFalse(usuarioImple.validaClave(claveIncorrecta));

    
  }

  @Test
  public void validaEmailTest2() {
    
	  String emailIncorrecto= "juan@";
	  
	  when(usuarioImple.validaClave(emailIncorrecto)).thenReturn(false);
	   assertEquals(false, usuarioImple.validaClave(emailIncorrecto));
       assertFalse(usuarioImple.validaClave(emailIncorrecto));
  }
}
