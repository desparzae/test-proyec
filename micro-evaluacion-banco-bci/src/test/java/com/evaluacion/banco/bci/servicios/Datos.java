package com.evaluacion.banco.bci.servicios;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;

import com.evaluacion.banco.bci.entity.Usuario;

public class Datos {

	
	public final static Usuario MOCK_USUARIO = new Usuario( "12345","Daniel", "daniel@test.cl", new Date(), "Daniel22");
	
	
	
	public final static ResponseEntity<?> MOCK_USUARIO_GUARDAR = ResponseEntity.status(HttpStatus.CREATED).body(new Usuario( "12345","Daniel", "daniel@test.cl", new Date(), "Daniel22"));
}
