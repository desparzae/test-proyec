package com.evaluacion.banco.bci.servicios;

import java.util.Date;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.UUID;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseStatus;

import com.evaluacion.banco.bci.controller.ControllerUsuario;
import com.evaluacion.banco.bci.entity.Usuario;
import com.evaluacion.banco.bci.repository.IUsuarioRepo;

import io.swagger.models.Response;

@Service
public class UsuarioServicioImple implements IUsuarioServicio {

	@Autowired
	public IUsuarioRepo usuarioRepo;
	private static final Logger LOGGER = LoggerFactory.getLogger(UsuarioServicioImple.class);
	@Override
	public List<Usuario> listaUsuarios() {

		return (List<Usuario>) usuarioRepo.findAll();
	}

	@Override
	public ResponseEntity<?> guardarUsuario(Usuario usuario) {
		Map<String, Object> respuesta = new HashMap<>();
		Usuario usuarioEncontrado = new Usuario();

		try {
			usuarioEncontrado = usuarioRepo.findByEmail(usuario.getEmail());

			if (usuarioEncontrado == null) {
				Usuario usuarioSave = new Usuario();

				
				
				if (!validaEmail(usuario.getEmail())) {
					
					respuesta.put("mensaje", "Email mal formado!");
					return ResponseEntity.status(HttpStatus.NOT_ACCEPTABLE).body(respuesta);
				}
				usuarioSave.setName(usuario.getName());
				usuarioSave.setEmail(usuario.getEmail());
				
				if (!validaClave(usuario.getPassword())) {

					respuesta.put("mensaje", "La password mal formado!");
					return ResponseEntity.status(HttpStatus.NOT_FOUND).body(respuesta);
				}
				usuarioSave.setPassword(usuario.getPassword());
				usuarioSave.setPhones(usuario.getPhones());
				usuarioSave.setLastLogin(new Date());
				usuarioSave.setToken(UUID.randomUUID().toString());
				LOGGER.info("Usuario registrado correctamente");
				return ResponseEntity.status(HttpStatus.CREATED).body(usuarioRepo.save(usuarioSave));
		}
			

		} catch (Exception e) {
			
				respuesta.put("mensaje", "Problemas internos!");
				return ResponseEntity.status(HttpStatus.CONFLICT).body(respuesta);
			

		}

	
		
		LOGGER.info("Correo se encuentra registrado!");
		respuesta.put("mensaje", "El correo ya registrado!");
		return ResponseEntity.status(HttpStatus.NOT_FOUND).body(respuesta);
	}

	@Override
	public Usuario updateUsuario(Usuario usuario) {
		// TODO Auto-generated method stub

		return usuarioRepo.save(usuario);
		
		
		
		
	}

	@Override
	public Usuario buscarUsuarioPorCorreo(String correo) {
	
		Usuario usuarioDb = usuarioRepo.findByEmail(correo);

		return usuarioDb;
	}

	public  Boolean validaEmail (String email) {
		Pattern pattern = Pattern.compile("^([0-9a-zA-Z]+[-._+&])*[0-9a-zA-Z]+@([-0-9a-zA-Z]+[.])+[a-zA-Z]{2,6}$");
		Matcher matcher = pattern.matcher(email);
		return matcher.matches();
	}
	

	public  Boolean validaClave (String clave) {
		Pattern pattern = Pattern.compile("^([A-Z]??[a-z]+?)[0-9]{2}?$");
		
		
		Matcher matcher = pattern.matcher(clave);
		return matcher.matches();
	}

	@Override
	public Optional<Usuario> buscarPorId(String id) {
		// TODO Auto-generated method stub
		return usuarioRepo.findById(id);
	}

}
