package com.evaluacion.banco.bci;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.domain.EntityScan;

import org.springframework.data.jpa.repository.config.EnableJpaRepositories;



@EntityScan(basePackages = "com")
@EnableJpaRepositories(basePackages =  "com")
@SpringBootApplication
public class MicroEvaluacionBancoBciApplication {

	public static void main(String[] args) {
		SpringApplication.run(MicroEvaluacionBancoBciApplication.class, args);
	}


}
