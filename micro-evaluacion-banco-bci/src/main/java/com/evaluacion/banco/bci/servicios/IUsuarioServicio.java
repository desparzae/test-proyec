package com.evaluacion.banco.bci.servicios;

import java.util.List;
import java.util.Optional;

import org.springframework.http.ResponseEntity;

import com.evaluacion.banco.bci.entity.Usuario;

public interface IUsuarioServicio {

	
	public List<Usuario> listaUsuarios();
	
	public ResponseEntity<?> guardarUsuario(Usuario usuario);
	
	public Usuario updateUsuario(Usuario usuario);
	
	public Usuario buscarUsuarioPorCorreo(String correo);

	public Optional<Usuario> buscarPorId(String id);
}
