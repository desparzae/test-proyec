package com.evaluacion.banco.bci.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import org.hibernate.annotations.GenericGenerator;

import com.fasterxml.jackson.annotation.JsonAlias;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.core.sym.Name;

import io.swagger.annotations.ApiModelProperty;

@Entity
@Table(name = "tbl_phone")
public class Phones {
	@JsonIgnore
	@Id
    @GeneratedValue(generator = "UUID")
    @GenericGenerator(name = "UUID",strategy = "org.hibernate.id.UUIDGenerator")
	@Column(name = "id", updatable = false, nullable = false)
	private String id;
	
	 
	 @ApiModelProperty(
	         dataType="String", example = "1234567"
	    )
	private  String number;
	
	 
	 @ApiModelProperty(
	         dataType="String", example = "1"
	    )
	@JsonProperty("citycode")
	@Column(name = "city_code")
	private String cityCode;

	
	 
	 @ApiModelProperty(
	         dataType="String", example = "57"
	    )
	@JsonProperty("contrycode")
	@Column(name = "contry_code")
	private String contryCode;
	

	@JsonIgnore
	@ManyToOne(fetch = FetchType.LAZY)
	   private Usuario usuario;
	  
	  
	  

	public Usuario getUsuario() {
		return usuario;
	}
	public void setUsuario(Usuario usuario) {
		this.usuario = usuario;
	}
	public String getId() {
		return id;
	}
	public void setId(String id) {
		this.id = id;
	}
	public String getNumber() {
		return number;
	}
	public void setNumber(String number) {
		this.number = number;
	}
	public String getCityCode() {
		return cityCode;
	}
	public void setCityCode(String cityCode) {
		this.cityCode = cityCode;
	}
	public String getContryCode() {
		return contryCode;
	}
	public void setContryCode(String contryCode) {
		this.contryCode = contryCode;
	}
	
	

}
