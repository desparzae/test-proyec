package com.evaluacion.banco.bci.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;


import com.evaluacion.banco.bci.entity.Usuario;

public interface IUsuarioRepo extends JpaRepository<Usuario, String>{
	
	
	Usuario findByEmail(String email);

}
