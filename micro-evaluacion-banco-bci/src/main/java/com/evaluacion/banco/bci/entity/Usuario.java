package com.evaluacion.banco.bci.entity;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.UUID;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.NotNull;

import org.hibernate.annotations.GenericGenerator;
import org.springframework.format.annotation.DateTimeFormat;

import com.fasterxml.jackson.annotation.JsonAlias;
import com.fasterxml.jackson.annotation.JsonProperty;

import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@AllArgsConstructor
@Data
@Entity
@Table(name = "tbl_usuario")
public class Usuario {
	@JsonProperty("id")
	@Id
	@GeneratedValue(generator = "UUID")
	@GenericGenerator(name = "UUID", strategy = "org.hibernate.id.UUIDGenerator")
	@Column(name = "id", updatable = false, nullable = false)
	private String id;
	@JsonProperty("name")
	@ApiModelProperty(dataType = "String", example = "Juan Rodriguez")
	private String name;
	
	
	@JsonProperty("email")
	@ApiModelProperty(dataType = "String", example = "juan@rodriguez.org")
	private String email;

	// fecha de creación del usuario
	
	@Column(name = "create_at")
	@Temporal(TemporalType.DATE)
	@DateTimeFormat(pattern = "yyyy-MM-dd")
	@JsonProperty("created")
	private Date createAt = new Date();

	@JsonProperty("password")
	@ApiModelProperty(dataType = "String", example = "hunter2")
	private String password;

	// del último ingreso (en caso de nuevo usuario, va a coincidir con lafecha de
	// creación)
	@JsonProperty("last_login")
	@Column(name = "last_login")
	private Date lastLogin;
	// fecha de la última actualización de usuario
	@JsonProperty("modified")
	@Column(name = "modified")
	private Date modified;

	// token de acceso de la API (puede ser UUID o JWT)
	@JsonProperty("token")
	private String token;

	// Indica si el usuario sigue habilitado dentro del sistema.
	@JsonProperty("isactive")
	private boolean isactive = true;
	@JsonProperty("phones")
	@OneToMany(mappedBy = "usuario", cascade = CascadeType.ALL, fetch = FetchType.LAZY)
	private List<Phones> phones = new ArrayList<>();

	public Date getCreateAt() {
		return createAt;
	}

	public void setCreateAt(Date createAt) {
		this.createAt = createAt;
	}

	public List<Phones> getPhones() {
		return phones;
	}

	public void setPhones(List<Phones> phones) {

		phones.forEach(p -> {
			p.setUsuario(this);
		});
		this.phones = phones;
	}

	@ApiModelProperty(hidden = true)
	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	@ApiModelProperty(hidden = true)
	public Date getLastLogin() {
		return lastLogin;
	}

	public void setLastLogin(Date lastLogin) {
		this.lastLogin = lastLogin;
	}

	@ApiModelProperty(hidden = true)
	public Date getModified() {
		return modified;
	}

	public void setModified(Date modified) {
		this.modified = modified;
	}

	@ApiModelProperty(hidden = true)
	public String getToken() {
		return token;
	}

	public void setToken(String token) {
		this.token = token;
	}

	@ApiModelProperty(hidden = true)
	public boolean isIsactive() {
		return isactive;
	}

	public void setIsactive(boolean isactive) {
		this.isactive = isactive;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	@Override
	public String toString() {
		return "Usuario [name=" + name + ", email=" + email + ", password=" + password + "]";
	}

	public Usuario() {
		super();
	}

	public Usuario(String id, String name, String email, Date createAt, String password) {
		super();
		this.id = id;
		this.name = name;
		this.email = email;
		this.createAt = createAt;
		this.password = password;
	}

}
