package com.evaluacion.banco.bci.controller;

import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Optional;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.client.HttpClientErrorException.NotFound;

import com.evaluacion.banco.bci.entity.Usuario;
import com.evaluacion.banco.bci.servicios.IUsuarioServicio;

import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;

@RestController
public class ControllerUsuario {
	private static final Logger LOGGER = LoggerFactory.getLogger(ControllerUsuario.class);
	@Autowired
	private IUsuarioServicio servicioUsuario;


	@ApiOperation(value = "Se registra usuario", nickname = "Registrar Usuario", notes = "", response = Usuario.class, tags = {
			"Resgistrar usuario", })
	@ApiResponses(value = { @ApiResponse(code = 404, message = "error 404", response = NotFound.class),
			@ApiResponse(code = 500, message = "error 500", response = Exception.class) })
	@RequestMapping(value = "/api/guardarUsuario", produces = { "application/json" }, method = RequestMethod.POST)
	ResponseEntity<?> almacenarUsuario(@Validated @RequestBody Usuario usuario) {
		LOGGER.info("Guardando usuario  {}", usuario);

		return servicioUsuario.guardarUsuario(usuario);
	}

	@ApiOperation(value = "Se registra usuario", nickname = "Registrar Usuario", notes = "", response = Usuario.class, tags = {
			"Lista usuario", })
	@ApiResponses(value = { @ApiResponse(code = 404, message = "error 404", response = NotFound.class),
			@ApiResponse(code = 500, message = "error 500", response = Exception.class) })
	@RequestMapping(value = "/api/listaUsuarios", produces = { "application/json" }, method = RequestMethod.POST)
	ResponseEntity<List<Usuario>> listaUsuarios() {

		return ResponseEntity.status(HttpStatus.OK).body(servicioUsuario.listaUsuarios());
	}

	@ApiOperation(value = "Se actualiza usuario requerido id del usuario", nickname = "Actualizar Usuario", notes = "", response = Usuario.class, tags = {
			"Actualiza usuario", })
	@ApiResponses(value = { @ApiResponse(code = 404, message = "error 404", response = NotFound.class),
			@ApiResponse(code = 500, message = "error 500", response = Exception.class) })
	@RequestMapping(value = "/api/actualizarUsuario", produces = { "application/json" }, method = RequestMethod.POST)
	ResponseEntity<?> actualizarUsuario(@RequestBody Usuario usuario) {

		LOGGER.info("Buscando usuario por correo {}", usuario);
		
		Optional<Usuario> userdb = servicioUsuario.buscarPorId(usuario.getId());

		Map<String, Object> respuesta = new HashMap<>();

		if (userdb.isEmpty()) {
			respuesta.put("mensaje", "El usuario no existe en la base de datos");
			return ResponseEntity.status(HttpStatus.NOT_FOUND).body(respuesta);
		}

		Usuario usuariodb = userdb.get();

		usuariodb.setEmail(usuario.getEmail());
		usuariodb.setName(usuario.getName());
		usuariodb.setModified(new Date());
		usuariodb.setPassword(usuario.getPassword());
		usuariodb.getPhones().clear();
		usuariodb.setPhones(usuario.getPhones());

		
		return  ResponseEntity.status(HttpStatus.OK).body(servicioUsuario.updateUsuario(usuariodb));
	}

	@ApiOperation(value = " Buscar usuario", nickname = "Buscar Usuario", notes = "", response = Usuario.class, tags = {
			"Buscar usuario", })
	@ApiResponses(value = { @ApiResponse(code = 404, message = "error 404", response = NotFound.class),
			@ApiResponse(code = 500, message = "error 500", response = Exception.class) })
	@GetMapping("api/buscarUsuarioPorCorreo/{correo}")
	ResponseEntity<?> buscarUsuario(@PathVariable String correo) {
		LOGGER.info("Buscando usuario por correo {}", correo);
		Map<String, Object> respuesta = new HashMap<>();
		Usuario usuarioDb = servicioUsuario.buscarUsuarioPorCorreo(correo);
		if (usuarioDb.equals(null)) {
			respuesta.put("mensaje", "El usuario no existe en la base de datos");
			return ResponseEntity.status(HttpStatus.NOT_FOUND).body(respuesta);
		}

		return ResponseEntity.status(HttpStatus.OK).body(usuarioDb);
	}

}
